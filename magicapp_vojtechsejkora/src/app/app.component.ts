import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pokus';

  firstNumber : string; 
  secondNumber : string;
  result : number;

  AddNumber(){
    this.result = parseInt(this.firstNumber) + parseInt(this.secondNumber)

  }
}
